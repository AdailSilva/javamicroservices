package com.adailsilva.hroauth.feignclients;

import java.util.List;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestParam;

import com.adailsilva.hroauth.entities.UserDetailsImpl;

@Component
@FeignClient(name = "hr-user", path = "/users")
public interface UserFeignClient {

	@GetMapping(value = "/names")
	List<String> allNames();

	@GetMapping(value = "/users")
	ResponseEntity<List<UserDetailsImpl>> findAll();

	@GetMapping(value = "/user/{id}")
	ResponseEntity<UserDetailsImpl> findById(@PathVariable(value = "id") Long id);

	@GetMapping(value = "/search")
	ResponseEntity<UserDetailsImpl> findByEmail(@RequestParam String email);

}
