package com.adailsilva.hroauth.resources;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.adailsilva.hroauth.entities.UserDetailsImpl;
import com.adailsilva.hroauth.services.UserService;

@RestController
@RequestMapping(value = "/users")
public class UserResource {

	@Autowired
	private UserService userService;

	@Autowired
	private UserDetailsService userDetailsService;

	@GetMapping(value = "/search")
	public ResponseEntity<UserDetailsImpl> findyByEmail(@RequestParam String email) {

		try {
			UserDetailsImpl user = userService.findByEmail(email);
			return ResponseEntity.ok(user);
		} catch (IllegalArgumentException | IllegalAccessException e) {
			return ResponseEntity.status(HttpStatus.NOT_FOUND).build();
		}

	}

	@GetMapping(value = "/search-user-details")
	public ResponseEntity<UserDetailsImpl> findyByUsername(@RequestParam String username) {

		try {
			UserDetailsImpl user = (UserDetailsImpl) userDetailsService.loadUserByUsername(username);
			return ResponseEntity.ok(user);
		} catch (IllegalArgumentException e) {
			return ResponseEntity.status(HttpStatus.NOT_FOUND).build();
		}

	}

}
