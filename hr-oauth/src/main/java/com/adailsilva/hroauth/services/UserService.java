package com.adailsilva.hroauth.services;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import com.adailsilva.hroauth.entities.UserDetailsImpl;
import com.adailsilva.hroauth.feignclients.UserFeignClient;

@Service
public class UserService implements UserDetailsService {

	private static Logger logger = LoggerFactory.getLogger(UserService.class);

	@Autowired
	private UserFeignClient userFeignClient;

	public UserDetailsImpl findByEmail(String email) throws IllegalAccessException {
		UserDetailsImpl user = userFeignClient.findByEmail(email).getBody();
		if (user == null) {
			logger.error("Email not found: " + email);
			throw new IllegalAccessException("Email not found");
		}
		logger.info("Email found: " + email);
		return user;
	}

	// Here the resource is automatically called
	@Override
	public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
		UserDetailsImpl user = userFeignClient.findByEmail(username).getBody();
		if (user == null) {
			logger.error("Username not found: " + username);
			throw new UsernameNotFoundException("UserDetailsImpl not found");
		}
		logger.info("Username found: " + username);
		return user;
	}

}
