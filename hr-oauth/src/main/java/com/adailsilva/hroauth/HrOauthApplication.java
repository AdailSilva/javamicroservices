package com.adailsilva.hroauth;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.circuitbreaker.EnableCircuitBreaker;
import org.springframework.cloud.netflix.eureka.EnableEurekaClient;
import org.springframework.cloud.openfeign.EnableFeignClients;
import org.springframework.core.env.Environment;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;

@EnableCircuitBreaker
//@RibbonClient(name = "hr-user")
@EnableFeignClients
@EnableEurekaClient
@SpringBootApplication
public class HrOauthApplication implements CommandLineRunner {

	@Autowired
	private BCryptPasswordEncoder bCryptPasswordEncoder;

	@Autowired
	private Environment environment;

	@Value("${spring.application.name}")
	private String springApplicationName;

//	@Value("${server.port}")
//	private String serverPort;

	@Value("${eureka.instance.instance-id}")
	private String eurekaInstanceInstanceId;

	@Value("${eureka.client.service-url.defaultZone}")
	private String eurekaClientServiceUrlDefaultZone;

	@Value("${hystrix.command.default.execution.isolation.thread.timeoutInMilliseconds}")
	private String hystrixCommandDefaultExecutionIsolationThreadTimeoutInMilliseconds;

	@Value("${ribbon.ConnectTimeout}")
	private String ribbonConnectTimeout;

	@Value("${ribbon.ReadTimeout}")
	private String ribbonReadTimeout;

	@Value("${adailsilva.config}")
	private String remoteConfiguration;
	
	@Value("${jwt.signingkey}")
	private String jwtSigningKey;
	
	@Value("${oauth.client.id}")
	private String oAuthClientId;

	@Value("${oauth.client.secret}")
	private String oAuthClientSecret;
	
	@Value("#{'${oauth.scopes}'.split(',')}")
	private List<String> oAuthScopes;
	
	@Value("${oauth.authorized.grant.types}")
	private String authorizedGrantTypes;
	
	@Value("${oauth.access.token.validity.seconds}")
	private int oAuthaccessTokenValiditySeconds;

	public static void main(String[] args) {
		SpringApplication.run(HrOauthApplication.class, args);
	}

	@Override
	public void run(String... args) throws Exception {
		System.out.println("Spring Application Name: " + springApplicationName);
		System.out.println("Server Port: " + environment.getProperty("local.server.port"));
		System.out.println("Eureka Instance Instance Id" + eurekaInstanceInstanceId);
		System.out.println("Eureka Client Service URL Default Zone: " + eurekaClientServiceUrlDefaultZone);
		System.out.println("Hystrix Command Default Execution Isolation Thread Timeout In Milliseconds: "
				+ hystrixCommandDefaultExecutionIsolationThreadTimeoutInMilliseconds);
		System.out.println("Ribbon Connect Timeout: " + ribbonConnectTimeout);
		System.out.println("Ribbon Read Timeout: " + ribbonReadTimeout);
		System.out.println("[ Remote Configuration ] --> " + remoteConfiguration);
		System.out.println("BCrypt: " + bCryptPasswordEncoder.encode("@Hacker101"));
		System.out.println("JWT Signing Key: " + jwtSigningKey);
		System.out.println("OAuth Client ID: " + oAuthClientId);
		System.out.println("OAuth Client Secret: " + oAuthClientSecret);
		oAuthScopes.stream().forEach(scope -> System.out.println("OAuth Scope: " + scope));
		System.out.println("OAuth Authorized Grant Types: " + authorizedGrantTypes);
		System.out.println("OAuth Access Token Validity Seconds: " + oAuthaccessTokenValiditySeconds);
	}

}
