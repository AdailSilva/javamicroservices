package com.adailsilva.hruser.resources;

import java.util.Arrays;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.adailsilva.hruser.entities.User;
import com.adailsilva.hruser.repositories.UserRepository;

@RestController
@RequestMapping(value = "/users")
public class UserResource {

	private static Logger logger = LoggerFactory.getLogger(UserResource.class);

	@Autowired
	private Environment environment;

	@Autowired
	private UserRepository userRepository;

	@GetMapping(value = "/names")
	public List<String> allNames() {

		logger.info("PORT = " + environment.getProperty("local.server.port"));

		return Arrays.asList("Adail Silva", "Ágatha Silva", "Davi Miguel");
	}

	@GetMapping(value = "/users")
	public ResponseEntity<List<User>> findAll() {
		List<User> users = userRepository.findAll();

		logger.info("PORT = " + environment.getProperty("local.server.port"));

		return ResponseEntity.ok(users);
	}

	@GetMapping(value = "/user/{id}")
	public ResponseEntity<User> findById(@PathVariable(value = "id") Long id) {

		// Hystrix Fault Tolerance
		// Requisition timeout simulation:
//		try {
//			Thread.sleep(3000L);
//		} catch (Exception e) {
//			e.printStackTrace();
//		}

		logger.info("PORT = " + environment.getProperty("local.server.port"));

		return userRepository.findById(id).map(ResponseEntity::ok).orElseGet(() -> ResponseEntity.notFound().build());
	}

	@GetMapping(value = "/search")
	public ResponseEntity<User> findByEmail(@RequestParam String email) {
		User user = userRepository.findByEmail(email);

		if (user != null) {

			logger.info("PORT = " + environment.getProperty("local.server.port"));

			return ResponseEntity.ok(user);
		}

		logger.info("PORT = " + environment.getProperty("local.server.port"));

		return ResponseEntity.notFound().build();
	}

}
