package com.adailsilva.hrapigatewayzuul.configurations;

import java.util.Arrays;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.web.servlet.FilterRegistrationBean;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.Ordered;
import org.springframework.http.HttpMethod;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.http.SessionCreationPolicy;
import org.springframework.security.oauth2.config.annotation.web.configuration.EnableResourceServer;
import org.springframework.security.oauth2.config.annotation.web.configuration.ResourceServerConfigurerAdapter;
import org.springframework.security.oauth2.config.annotation.web.configurers.ResourceServerSecurityConfigurer;
import org.springframework.security.oauth2.provider.token.store.JwtTokenStore;
import org.springframework.web.cors.CorsConfiguration;
import org.springframework.web.cors.CorsConfigurationSource;
import org.springframework.web.cors.UrlBasedCorsConfigurationSource;
import org.springframework.web.filter.CorsFilter;

@Configuration
@EnableWebSecurity
@EnableResourceServer
public class ResourceServerConfig extends ResourceServerConfigurerAdapter {

	@Autowired
	private JwtTokenStore jwtTokenStore;

	private static final String[] PUBLIC = { "/router-hr-oauth/oauth/token" };

	private static final String[] OPERATOR = { "/router-hr-worker/**" };

	private static final String[] ADMIN = { "/router-hr-api-gateway-zuul/**", "/router-hr-eureka-server/**",
			"/router-hr-oauth/**", "/router-hr-payroll/**", "/router-hr-user/**", "/router-hr-worker/**" };

//	@Autowired
//	public void configure(AuthenticationManagerBuilder auth) throws Exception {
//		auth.inMemoryAuthentication().withUser("admin@adailsilva.com").password("@Hacker101").roles("ADMIN");
//	}

	@Override
	public void configure(ResourceServerSecurityConfigurer resources) throws Exception {
		resources.tokenStore(jwtTokenStore);
		resources.stateless(true);
	}

	@Override
	public void configure(HttpSecurity http) throws Exception {
		// In the database each Role will have to be added with the ROLE_ prefix, in
		// .hasAnyRole this is unnecessary.
		// Example: ROLE_ADMINISTRATOR this ADMINISTRATOR.
		http.authorizeRequests()
			.antMatchers(PUBLIC)
			.permitAll()
			.antMatchers(HttpMethod.GET, OPERATOR)
			.hasAnyRole("ADMINISTRATOR", "OPERATOR")
			.antMatchers(ADMIN)
			.hasAnyRole("ADMINISTRATOR")
			.anyRequest()
			.authenticated()
			.and()
			.sessionManagement()
			.sessionCreationPolicy(SessionCreationPolicy.STATELESS)
			.and()
			.csrf()
			.disable();
		
		http.cors()
			.configurationSource(corsConfigurationSource());
	}

	@Bean
	public CorsConfigurationSource corsConfigurationSource() {
		CorsConfiguration corsConfiguration = new CorsConfiguration();
		corsConfiguration.setAllowedOrigins(Arrays.asList("*"));
		corsConfiguration.setAllowedMethods(Arrays.asList("POST", "GET", "PUT", "DELETE", "PATCH"));
		corsConfiguration.setAllowCredentials(true);
		corsConfiguration.setAllowedHeaders(Arrays.asList("Authorization", "Content-Type"));

		UrlBasedCorsConfigurationSource urlBasedCorsConfigurationSource = new UrlBasedCorsConfigurationSource();
		urlBasedCorsConfigurationSource.registerCorsConfiguration("/**", corsConfiguration);
		return urlBasedCorsConfigurationSource;
	}

	@Bean
	public FilterRegistrationBean<CorsFilter> corFilterRegistrationBean() {		
		FilterRegistrationBean<CorsFilter> bean = new FilterRegistrationBean<>(new CorsFilter(corsConfigurationSource()));
		bean.setOrder(Ordered.HIGHEST_PRECEDENCE);		
		return bean;
	}
}
