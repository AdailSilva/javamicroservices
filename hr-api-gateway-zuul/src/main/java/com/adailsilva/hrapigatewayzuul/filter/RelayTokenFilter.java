package com.adailsilva.hrapigatewayzuul.filter;

import java.util.Set;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

import com.netflix.zuul.ZuulFilter;
import com.netflix.zuul.context.RequestContext;

@Component
public class RelayTokenFilter extends ZuulFilter {

	// Solution for: Zuul proxy oAuth2 Unauthorized in Spring Boot
	// To: sensitiveHeaders: Cookie,Set-Cookie and ignoredHeaders: authorization

	private final Logger logger = LoggerFactory.getLogger(this.getClass());

	@Override
	public String filterType() {
		return "pre";
	}

	@Override
	public int filterOrder() {
		return 10000;
	}

	@Override
	public boolean shouldFilter() {
		return true;
	}

	@Override
	public Object run() {
		RequestContext requestContext = RequestContext.getCurrentContext();
		logger.info("In zuul filter " + requestContext.getRequest().getRequestURI());
		System.out.println("Authorization: " + requestContext.getRequest().getHeader("authorization"));

//		String inputString = "angular:@Hacker101";
//		Charset charset = Charset.forName("UTF-8");
//		byte[] encoded = inputString.getBytes(charset);
//		requestContext.addZuulRequestHeader("Authorization", "Basic " + new String(encoded));

//		HttpServletRequest httpServletRequest = requestContext.getRequest();
		if (requestContext.getRequest().getHeader("authorization") == null) {
//			requestContext.unset();
//			requestContext.setResponseStatusCode(HttpStatus.UNAUTHORIZED.value());

//			String inputString = "angular:@Hacker101";
//			Charset charset = Charset.forName("UTF-8");
//			byte[] encoded = inputString.getBytes(charset);
//			requestContext.set("authorization",  "Basic " + new String(encoded));
//			requestContext.addZuulRequestHeader("authorization", "Basic " + new String(encoded));
			System.out.println("Get Zuul Request Headers: " + requestContext.getZuulRequestHeaders());
		} else {
			@SuppressWarnings("unchecked")
			Set<String> ignoredHeaders = (Set<String>) requestContext.get("ignoredHeaders");
			System.out.println("Ignored Headers BEFORE: " + requestContext.get("ignoredHeaders"));
			// We need our JWT tokens relayed to resource servers
			ignoredHeaders.remove("authorization");
//			ignoredHeaders.remove("cookie");
//			ignoredHeaders.remove("set-cookie");
			System.out.println("Ignored Headers AFTER: " + requestContext.get("ignoredHeaders"));

			/*
			 * Result:
			 * 
			 * Ignored Headers BEFORE: [authorization, set-cookie, expires,
			 * x-content-type-options, x-xss-protection, cookie, x-frame-options,
			 * cache-control, pragma]
			 * 
			 * Ignored Headers AFTER: [set-cookie, expires, x-content-type-options,
			 * x-xss-protection, cookie, x-frame-options, cache-control, pragma]
			 * 
			 */
		}
		return null;
	}
}
