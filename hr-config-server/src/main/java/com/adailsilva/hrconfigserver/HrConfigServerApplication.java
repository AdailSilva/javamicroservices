package com.adailsilva.hrconfigserver;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.config.server.EnableConfigServer;
import org.springframework.cloud.context.config.annotation.RefreshScope;
import org.springframework.core.env.Environment;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;

@RefreshScope // For all classes that have access to some custom configuration
@EnableConfigServer
@SpringBootApplication
public class HrConfigServerApplication implements CommandLineRunner {
	
	@Autowired
	private BCryptPasswordEncoder bCryptPasswordEncoder;
	
	@Autowired
	private Environment environment;
	
	@Value("${spring.application.name}")
	private String springApplicationName;
	
//	@Value("${server.port}")
//	private String serverPort;
	
	@Value("${spring.cloud.config.server.git.uri}")
	private String springCloudConfigServerGitUri;
	
	@Value("${spring.cloud.config.server.git.default-label}")
	private String springCloudConfigServerGitDefaultLabel;
	
	@Value("${spring.cloud.config.server.git.username}")
	private String springCloudConfigServerGitUsername;
	
	@Value("${spring.cloud.config.server.git.password}")
	private String springCloudConfigServerGitPassword;

	public static void main(String[] args) {
		SpringApplication.run(HrConfigServerApplication.class, args);
	}

	@Override
	public void run(String... args) throws Exception {
		System.out.println("Spring Application Name: " + springApplicationName);
		System.out.println("Server Port: " + environment.getProperty("local.server.port"));
		System.out.println("Spring Cloud Config Server Git URI: " + springCloudConfigServerGitUri);
		System.out.println("Spring Cloud Config Server Git Default Label: " + springCloudConfigServerGitDefaultLabel);
		System.out.println("Spring Cloud Config Server Git Username: " + springCloudConfigServerGitUsername);
		System.out.println("Spring Cloud Config Server Git Password: " + springCloudConfigServerGitPassword);
		System.out.println("BCrypt: " + bCryptPasswordEncoder.encode("@Hacker101"));
	}

}
