package com.adailsilva.hrpayroll.feignclients;

import java.util.List;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestParam;

import com.adailsilva.hrpayroll.entities.Worker;

@Component
@FeignClient(name = "hr-worker", path = "/workers")
public interface WorkerFeignClient {

	@GetMapping(value = "/names")
	List<String> allNames();

	@GetMapping(value = "/workers")
	ResponseEntity<List<Worker>> findAll();;

	@GetMapping(value = "/worker/{id}")
	ResponseEntity<Worker> findById(@PathVariable(value = "id") Long id);

	@GetMapping(value = "/search")
	ResponseEntity<Worker> findByName(@RequestParam String name);

}
