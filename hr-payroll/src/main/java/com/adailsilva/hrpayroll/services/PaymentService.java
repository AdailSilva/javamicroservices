package com.adailsilva.hrpayroll.services;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import com.adailsilva.hrpayroll.entities.Payment;
import com.adailsilva.hrpayroll.entities.Worker;
import com.adailsilva.hrpayroll.feignclients.WorkerFeignClient;

@Service
public class PaymentService {

	private static Logger logger = LoggerFactory.getLogger(PaymentService.class);

	@Autowired
	private WorkerFeignClient workerFeignClient;

	public ResponseEntity<Payment> getPayment(Long workerId, Integer days) throws IllegalAccessException {

		try {
			Worker worker = workerFeignClient.findById(workerId).getBody();
			if (worker == null) {
				logger.error("Worker not found: " + worker);
				throw new IllegalAccessException("Worker not found");
			}
			
			Payment payment = new Payment(worker.getName(), worker.getDailyIncome(), days);
			logger.info("Worker found: " + worker);
			return ResponseEntity.ok(payment);
		} catch (Exception e) {
			return ResponseEntity.notFound().build();
		}

	}
}
